import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // mock data
    categories: [
      {
        name: 'Sofas',
        picture:
        'https://images.unsplash.com/photo-1493663284031-b7e3aefcae8e?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=750&amp;q=80 750w',
        uuid: 'fbddd44a-fc01-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Chairs',
        picture:
        'https://images.unsplash.com/photo-1505797149-43b0069ec26b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjM0MTM2fQ&auto=format&fit=crop&w=375&q=80 375w',
        uuid: 'fbddd80a-fc01-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Beds',
        picture:
        'https://images.unsplash.com/photo-1540518614846-7eded433c457?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=739&q=80 739w',
        uuid: 'fbddd6c0-fc01-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Lights',
        picture:
        'https://images.unsplash.com/photo-1507494924047-60b8ee826ca9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=666&q=80 666w',
        uuid: 'fbddd940-fc01-11e9-8f0b-362b9e155667',
      },
      {
        name: 'No Category',
        picture:
          'https://images.unsplash.com/photo-1554995207-c18c203602cb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80 750w',
        uuid: '101f9589-652d-4207-afa0-4df63a590e82',
      },
    ],
    products: [
      {
        name: 'Hammarn',
        category: 'Sofas',
        price: 350,
        description: 'Cuddle sofa',
        picture: 'https://images.unsplash.com/photo-1519961655809-34fa156820ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'a4005f0b-0138-4d2b-8c77-7fcf29f48743',
      },
      {
        name: 'Knopparp',
        category: 'Sofas',
        price: 80,
        description: '3-seat-sofa',
        picture: 'https://images.unsplash.com/photo-1540574163026-643ea20ade25?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80 750w',
        productUuid: '76b837cb-32eb-46bf-8b7c-c4737f5653b5',
      },
      {
        name: 'Gunde',
        category: 'Chairs',
        price: 6,
        description: 'Folding chair',
        picture: 'https://cdn.pixabay.com/photo/2018/01/01/19/18/chair-3054780_960_720.jpg',
        productUuid: '980706a4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Strandmon',
        category: 'Chairs',
        price: 165,
        description: 'Simple chair',
        picture: 'https://cdn.pixabay.com/photo/2016/06/28/15/25/chair-1484853_960_720.jpg',
        productUuid: '98070906-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Slattum',
        category: 'Beds',
        price: 445,
        description: 'Japanese double bed',
        picture: 'https://images.unsplash.com/photo-1566665797739-1674de7a421a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80 667w',
        productUuid: '98070ac8-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Arstid',
        category: 'Lights',
        price: 15,
        description: 'Table lamp',
        picture: 'https://images.unsplash.com/photo-1507473885765-e6ed057f782c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'b02b27c4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Hektar',
        category: 'Lights',
        price: 25,
        description: 'Nightstands lamp',
        picture: 'https://images.unsplash.com/photo-1455792244736-3ed96c3d7f7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80 375w',
        productUuid: 'b02b2aa8-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Hammarn',
        category: 'Sofas',
        price: 350,
        description: 'Cuddle sofa',
        picture: 'https://images.unsplash.com/photo-1519961655809-34fa156820ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'a1005f0b-0138-4d2b-8c77-7fcf29f48743',
      },
      {
        name: 'Knopparp',
        category: 'Sofas',
        price: 80,
        description: '3-seat-sofa',
        picture: 'https://images.unsplash.com/photo-1540574163026-643ea20ade25?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80 750w',
        productUuid: '71b837cb-32eb-46bf-8b7c-c4737f5653b5',
      },
      {
        name: 'Gunde',
        category: 'Chairs',
        price: 6,
        description: 'Folding chair',
        picture: 'https://cdn.pixabay.com/photo/2018/01/01/19/18/chair-3054780_960_720.jpg',
        productUuid: '910706a4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Strandmon',
        category: 'Chairs',
        price: 165,
        description: 'Simple chair',
        picture: 'https://cdn.pixabay.com/photo/2016/06/28/15/25/chair-1484853_960_720.jpg',
        productUuid: '91070906-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Slattum',
        category: 'Beds',
        price: 445,
        description: 'Japanese double bed',
        picture: 'https://images.unsplash.com/photo-1566665797739-1674de7a421a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80 667w',
        productUuid: '91070ac8-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Arstid',
        category: 'Lights',
        price: 15,
        description: 'Table lamp',
        picture: 'https://images.unsplash.com/photo-1507473885765-e6ed057f782c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'b12b27c4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Hektar',
        category: 'Lights',
        price: 25,
        description: 'Nightstands lamp',
        picture: 'https://images.unsplash.com/photo-1455792244736-3ed96c3d7f7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80 375w',
        productUuid: 'b12b2aa8-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Hammarn',
        category: 'Sofas',
        price: 350,
        description: 'Cuddle sofa',
        picture: 'https://images.unsplash.com/photo-1519961655809-34fa156820ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'a2005f0b-0138-4d2b-8c77-7fcf29f48743',
      },
      {
        name: 'Knopparp',
        category: 'Sofas',
        price: 80,
        description: '3-seat-sofa',
        picture: 'https://images.unsplash.com/photo-1540574163026-643ea20ade25?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80 750w',
        productUuid: '72b837cb-32eb-46bf-8b7c-c4737f5653b5',
      },
      {
        name: 'Gunde',
        category: 'Chairs',
        price: 6,
        description: 'Folding chair',
        picture: 'https://cdn.pixabay.com/photo/2018/01/01/19/18/chair-3054780_960_720.jpg',
        productUuid: '920706a4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Strandmon',
        category: 'Chairs',
        price: 165,
        description: 'Simple chair',
        picture: 'https://cdn.pixabay.com/photo/2016/06/28/15/25/chair-1484853_960_720.jpg',
        productUuid: '92070906-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Slattum',
        category: 'Beds',
        price: 445,
        description: 'Japanese double bed',
        picture: 'https://images.unsplash.com/photo-1566665797739-1674de7a421a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80 667w',
        productUuid: '92070ac8-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Arstid',
        category: 'Lights',
        price: 15,
        description: 'Table lamp',
        picture: 'https://images.unsplash.com/photo-1507473885765-e6ed057f782c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'b22b27c4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Hektar',
        category: 'Lights',
        price: 25,
        description: 'Nightstands lamp',
        picture: 'https://images.unsplash.com/photo-1455792244736-3ed96c3d7f7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80 375w',
        productUuid: 'b22b2aa8-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Hammarn',
        category: 'Sofas',
        price: 350,
        description: 'Cuddle sofa',
        picture: 'https://images.unsplash.com/photo-1519961655809-34fa156820ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'a3005f0b-0138-4d2b-8c77-7fcf29f48743',
      },
      {
        name: 'Knopparp',
        category: 'Sofas',
        price: 80,
        description: '3-seat-sofa',
        picture: 'https://images.unsplash.com/photo-1540574163026-643ea20ade25?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80 750w',
        productUuid: '73b837cb-32eb-46bf-8b7c-c4737f5653b5',
      },
      {
        name: 'Gunde',
        category: 'Chairs',
        price: 6,
        description: 'Folding chair',
        picture: 'https://cdn.pixabay.com/photo/2018/01/01/19/18/chair-3054780_960_720.jpg',
        productUuid: '930706a4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Strandmon',
        category: 'Chairs',
        price: 165,
        description: 'Simple chair',
        picture: 'https://cdn.pixabay.com/photo/2016/06/28/15/25/chair-1484853_960_720.jpg',
        productUuid: '93070906-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Slattum',
        category: 'Beds',
        price: 445,
        description: 'Japanese double bed',
        picture: 'https://images.unsplash.com/photo-1566665797739-1674de7a421a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80 667w',
        productUuid: '93070ac8-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Arstid',
        category: 'Lights',
        price: 15,
        description: 'Table lamp',
        picture: 'https://images.unsplash.com/photo-1507473885765-e6ed057f782c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80 334w',
        productUuid: 'b32b27c4-fbf6-11e9-8f0b-362b9e155667',
      },
      {
        name: 'Hektar',
        category: 'Lights',
        price: 25,
        description: 'Nightstands lamp',
        picture: 'https://images.unsplash.com/photo-1455792244736-3ed96c3d7f7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80 375w',
        productUuid: 'b32b2aa8-fbf6-11e9-8f0b-362b9e155667',
      },
    ],
    shoppingBag: [],
  },
  mutations: {
    changeAmountOfItemsInShoppingBag({ products, shoppingBag }, { productUuid, amountOfItems }) {
      const amountOfItemsAsNumber = parseInt(amountOfItems);
      const indexOfProductInShoppingBag = shoppingBag.findIndex(
        product => product.productUuid === productUuid, productUuid,
      );
      const productIndex = products.findIndex(
        product => product.productUuid === productUuid, productUuid,
      );
      if (indexOfProductInShoppingBag === -1) {
        const item = JSON.parse(JSON.stringify(products[productIndex]));
        item.amount = amountOfItemsAsNumber;
        shoppingBag.push(item);
      } else if (amountOfItemsAsNumber === 0) {
        shoppingBag.splice(indexOfProductInShoppingBag, 1);
      } else if (amountOfItemsAsNumber){
        // eslint-disable-next-line no-param-reassign
        shoppingBag[indexOfProductInShoppingBag].amount = amountOfItemsAsNumber;
      } else {
        shoppingBag[indexOfProductInShoppingBag].amount = 1;
      }
    },
    addNumberOfItemsToShoppingBag({ shoppingBag }, { productUuid, amountOfItems }) {
      const indexOfProductInShoppingBag = shoppingBag.findIndex(
        product => product.productUuid === productUuid, productUuid,
      );
      let totalAmountOfItems;
      if (indexOfProductInShoppingBag !== -1) {
        totalAmountOfItems = shoppingBag[indexOfProductInShoppingBag].amount;
        totalAmountOfItems += amountOfItems;
      } else {
        totalAmountOfItems = amountOfItems;
      }
      this.commit('changeAmountOfItemsInShoppingBag', {
        productUuid,
        amountOfItems: totalAmountOfItems,
      });
    },
  },
  actions: {
    addSingleItemToShoppingBag({ commit }, { productUuid }) {
      commit('addNumberOfItemsToShoppingBag', { productUuid, amountOfItems: 1 });
    },
  },
});
